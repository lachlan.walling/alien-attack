export async function handler(event, context ) {
  switch(event.RequestType) {
    case 'Create':
      return { PhysicalResourceId: physicalId };
    case 'Update':
      return;
    case 'Delete':
      return;
    default:
      throw new Error('Invalid event');
  }
}